__author__ = 'tanu'

import os, sys

import code
import logging
import readline
import requests
import atexit
import commands
import inspect

import configparser

from leveldb import LevelDB, LevelDBError
from globusonline.catalog.client.dataset_client import DatasetClient
from globusonline.transfer.api_client import create_client_from_args 
from globusonline.catalog.client.goauth import get_access_token

import sciunit
from sciunit.client.completer import BufferAwareCompleter
from sciunit.client.query_dataset_client import get_catalogs, get_catalog_by_name, get_last_datasets

from sciunit.client.commands.util import UNDEFINED, SafeList, run_command, is_sciunit_selected
from sciunit.client.commands.start import parse_cmd_sciunit

from sciunit.client.commands.annotate import parse_cmd_annotate
from sciunit.client.commands.addmember import parse_cmd_add_member
from sciunit.client.commands.package import parse_cmd_package
from sciunit.client.commands.transfer import parse_cmd_transfer
from sciunit.client.commands.track import parse_cmd_track

import warnings
warnings.filterwarnings("ignore")

global datasetClient

def init_DatasetClient(goauth_token,BASE_URL):
    datasetClient = DatasetClient(goauth_token, BASE_URL)
    if datasetClient is None:
        print "cannot obtain a valid dataset client"
        exit(1)

class SDConfig:
    config_file_name = None
    config = None

    ## Read the config.ini file and check if URL is set
    ## if not ask to set it and exit
    ## if username is none ask for username and store it config.ini.
    ## Next time the client is run, read username from config.ini
    ## If Globus token is none, Obtain Globus token and store it, else proceed
    ## Return config
    def __init__(self):
        
	#SSL check
	os.system('export PYTHONHTTPSVERIFY=0')

	self.config_file_name = os.path.join(os.path.expanduser("~"),'.sciunit','config.ini')
        self.config = configparser.ConfigParser()
        try:
            self.config.read_file(open(self.config_file_name))
        except:
	    #TODO: check if no GCP is running; If yes, ask user to stop it
	

            self.config['Default'] = {'url': "https://ec2-54-152-190-179.compute-1.amazonaws.com/service/dataset",
                                   'uname' : 'None',
                                   'goauth-token' : 'None'}
            self.config['GLOBUS'] = {
                'local-folder': '~/.sciunit/docker_images', # the default location for images
                'local-endpoint': 'None',		# this will be set if user makes add_member or transfer operations.
		'setupkey': 'None',
                'remote-endpoint': 'tanum#geodataserver', # the GeoTrust Server endpoint
                'globus-remote-folder': '/home/ubuntu/globus/tanum', #the default location on GeoTrust Server
                'globus-local-folder': '~/.sciunit/docker_images', # the default localtion for images
                }
            self.config['Sciunit'] = {'catalog' : 11}
            self.write_cfg_file()

        if self.get_cfg_field('URL') == "None":
            print "Sciunit URL is not set in %s"%self.config_file_name
            exit(1)

        b_will_exit = False
        if  self.get_cfg_field('uname') == "None":
            uname = raw_input("Please provide user name > ")
        else:
            uname = self.get_cfg_field('uname')

        if self.get_cfg_field('goauth-token') == "None":
            b_will_exit = True
            try:
                result = get_access_token(username=uname)
                self.config['Default']['uname']=uname
                self.config['Default']['goauth-token']= result.token
                b_will_exit = False
            except Exception as e:
                print "There was an error in obtaining Globus token. Please check username or your password."
                sys.stderr.write(str(e) + "\n")
	else:
		result = self.get_cfg_field('goauth-token')

	if self.config['GLOBUS']['local-endpoint'] == "None":
	    localendpoint = raw_input("Please provide local-endpoint name > ")
	    # get hostname
	    #_,hostname = commands.getstatusoutput('dig +short myip.opendns.com @resolver1.opendns.com')
	
	    # set globus params
	    # Note: this is the only way endoint creation seem to work
	    client_args = [self.config['Default']['uname'], '-g', self.config['Default']['goauth-token']]
	    try:
	    
	    	api,_ = create_client_from_args(client_args) 
   	    	code, reason, data = api.endpoint_create(localendpoint,is_globus_connect=True)
	    	if (code == 201):
		   self.config['GLOBUS']['local-endpoint'] = uname + "#" + localendpoint
    	    	   setupkey = data.get("globus_connect_setup_key")
    	    	   if setupkey:
        	   	print "GC Setup Key: %s" % setupkey
		   	if self.config['GLOBUS']['setupkey'] == "None":
		    	    self.config['GLOBUS']['setupkey'] = setupkey
		   print data["message"]
    		
		current_path = os.path.dirname(inspect.getfile(sciunit))
		globus_path = os.path.join(current_path, "client/commands/bin/globusconnectpersonal-*/globusconnectpersonal")
		cmd_to_run = globus_path + "  -setup " + self.config['GLOBUS']['setupkey']
		os.system(cmd_to_run)

	    except Exception as e:
		print e
		print "There was an error setting up the endpoint. Please check username or your password."	
	else:
	    localendpoint = self.config['GLOBUS']['local-endpoint']
	    setupkey = self.config['GLOBUS']['setupkey']

        if b_will_exit:
            print "Thank you for setup. Please run the client again."
            exit(1)

        self.write_cfg_file()


    def write_cfg_file(self):
        with open(self.config_file_name, 'w') as configfile:
            self.config.write(configfile)

    def get_cfg_field(self, field, namespace='Default'):
        try:
            return str(self.config.get(namespace, field))
        except:
            print "%s is not defined"% field
            return "None"

    def gd_init_catalog(self,datasetClient):
        catalog_id = self.get_cfg_field('catalog',namespace='Sciunit')
        if  catalog_id == "None":
            nr_tries = 0
            while (nr_tries<3 and catalog_id == "None"):
                catalog_name = raw_input("Please provide catalog name > ")
                # Show the data to user and get catalog_name from user
                catalog_json = get_catalog_by_name(datasetClient,catalog_name)
                if catalog_json is not None:
                    catalog_id = str(catalog_json.get('id',None))
                    self.config['Sciunit']['catalog'] = catalog_id
                else:
                    print "Could not find catalog with name containing '%s'"%catalog_name
                nr_tries += 1

            self.write_cfg_file()
        return catalog_id


    def init_GlobusConnectPersonal(self):
    	current_path = os.path.dirname(inspect.getfile(sciunit))
	globus_path = os.path.join(current_path, "client/commands/bin/globusconnectpersonal-*/globusconnectpersonal")
	cmd_to_run = "%s -start &"%globus_path
	output = os.system(cmd_to_run)
 

if __name__ == '__main__':
    home_folder = os.path.expanduser("~")
    dot_sciunit_folder = os.path.join(home_folder,'.sciunit')
    if not os.path.exists(dot_sciunit_folder):
        os.makedirs(dot_sciunit_folder)

    # Read configuration file
    cfg = SDConfig()
    
    ### Init Globus
    cfg.init_GlobusConnectPersonal()
		
    ## Init a datasetclient
    datasetClient = DatasetClient(cfg.config['Default']['goauth-token'],cfg.config['Default']['URL'])
    
    ## If datasetclient is not None then connection between client and server established.
    if datasetClient is None:
        print "Cannot obtain a valid dataset client"
        exit(1)

    mycatalog_id = cfg.gd_init_catalog(datasetClient)
    #print "mycatalog_id=",mycatalog_id

    dataset_list = get_last_datasets(datasetClient,mycatalog_id)
    dataset_dict = {key:{} for key in dataset_list}

    ## check if the LevelDB local database and histfile exists; if not create; if yes re-use	
    ## LevelDB local database

    levelDB_local_database = os.path.join(dot_sciunit_folder,".sciunit_levelDB")
    docker_image_id = None
    ## Add history
    histfile = os.path.join(dot_sciunit_folder,".sciunit_history")
    try:
        readline.read_history_file(histfile)
    except IOError:
        pass
    atexit.register(readline.write_history_file, histfile)
    del histfile

    db = LevelDB(levelDB_local_database)
    if db == None:
        print("Cannot open db from "+levelDB_local_database)
        exit(1)

    print ('Enter "stop" to end session')
    completer_suggestions = {
        'sciunit':{'start':dataset_dict, 'delete':{}},
        'add_member':{},
        'track':{},
        'transfer':{},
        'package':{'provenance':
                         {'level':{'self':{}, 'collaboration':{}, 'community':{}}
                          },
                     'level':{'self':{}, 'collaboration':{}, 'community':{}},
                     'list':{},
                     #'add':{}, #Commented by TM on 03-13-17
                     'delete':{},
                     },
        'annotate':{'sciunit':
                         {'geoprop1':{}, 'prop2':{}, 'fluid':{}
                          },
                     'member':{}
                     },
        'stop':{}
    }
    sciunit_client_special_string = '--'
    print ("Commands start with  "+sciunit_client_special_string)
    comp=BufferAwareCompleter(completer_suggestions,sciunit_client_special_string)
    readline.set_completer_delims(' \t\n')
    if 'libedit' in readline.__doc__:
        readline.parse_and_bind("bind ^I rl_complete")
    else:
        readline.parse_and_bind("tab: complete")
    readline.set_completer(comp.complete)

    sciunit_name = UNDEFINED
    sciunit_id = None
    while True :
        raw_cmd = raw_input(sciunit_name+" > ")
        cmd_to_run = raw_cmd.strip()
        cmd_splitted = SafeList(raw_cmd.split())
        first_command = cmd_splitted.get(0,'')

        if first_command.upper() in ['--STOP','X']:
            break
        elif first_command == "--sciunit":
            sciunit_name, sciunit_id, err_message = parse_cmd_sciunit(cmd_splitted, mycatalog_id, sciunit_id, datasetClient)
            if err_message != "":
                print err_message

        elif first_command == "--track":
            if is_sciunit_selected(sciunit_id):
                parse_cmd_track(cmd_splitted)

        elif first_command == "--transfer":
            if is_sciunit_selected(sciunit_id):
                parse_cmd_transfer(cmd_splitted,package_hash, cfg)

        elif first_command == "--package":
            package_hash = parse_cmd_package(cmd_splitted, mycatalog_id, sciunit_id, datasetClient, db, cfg)

        elif first_command in ["--annotate", "--add_member"]:
            locals()["parse_cmd_"+first_command[2:]](cmd_splitted, mycatalog_id, sciunit_id, datasetClient, db, cfg)

        elif first_command == "cd":
            try:
                os.chdir(cmd_splitted.get(1,home_folder))
            except Exception as e:
                sys.stderr.write(str(e) + "\n")

        else:
            # any bash command that we want to pass to the system
            run_command(cmd_to_run)
    print "done"



