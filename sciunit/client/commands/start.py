from sciunit.client.commands.util import UNDEFINED

#######################################
#   Parse sciunit command
#   returns  (sciunit_name, sciunit_id, err_message)
#######################################
def parse_cmd_sciunit(cmd_splitted, catalog_id, sciunit_id, datasetClient):
    cmd_2 = cmd_splitted.get(1,"")
    if cmd_2 == "start":
        sciunit_name = cmd_splitted.get(2,UNDEFINED)
        if sciunit_name != UNDEFINED:
            sciunit_name = sciunit_name

            r, datasets = datasetClient.get_datasets(catalog_id)
            filtered_datasets = [x for x in datasets if x['name']==sciunit_name]
            if len(filtered_datasets)<1:
                r, data = datasetClient.create_dataset(catalog_id,dict(name=sciunit_name))
                sciunit_id = data['id']
            elif len(filtered_datasets)== 1:
                sciunit_id = filtered_datasets[0]['id']
            else:
                return UNDEFINED, None, "please choose another name; this one seems corrupted"

            return sciunit_name, sciunit_id, ""
        else:
            return UNDEFINED, None, "cannot understand sciunit name"

    elif cmd_2 == "delete":
        pass
        #if sciunit_id is None:
        #    return None, None, "cannot use sciunit name"

    else:
        # sciunit something
        return UNDEFINED, None, "usage: sciunit [start <gounit name>|delete]"

    return UNDEFINED, None, ""

