from sciunit.client.commands.util import UNDEFINED,run_command

import re
import os
import urllib
from sys import platform as _platform
from globusonline.transfer.api_client import Transfer, create_client_from_args
from datetime import datetime, timedelta

def display_task(api,task_id, show_successful_transfers=True):
    code, reason, data = api.task(task_id)
    print "Task %s:" % task_id
    _print_task(data, 0)

    if show_successful_transfers:
        code, reason, data = api.task_successful_transfers(task_id)
        transfer_list = data["DATA"]
        print "Successful Transfers (src -> dst)"
        for t in transfer_list:
            print " %s -> %s" % (t[u'source_path'],
                                 t[u'destination_path'])



def wait_for_task(api, task_id, timeout=120, poll_interval=30):
    """
    Wait for a task to complete within @timeout seconds, polling
    every @poll_interval seconds. If the task completed in the timeout,
    return the status ("SUCCEEDED" or "FAILED"). If it did not complete,
    returns None. Caller is responsible for cancelling incomplete task
    as appropriate.
    """
    assert timeout % poll_interval == 0, \
        "timeout must be multiple of poll_interval"
    timeout_left = timeout
    while timeout_left >= 0:
        code, reason, data = api.task(task_id, fields="status")
        status = data["status"]
        if status in ("SUCCEEDED", "FAILED"):
            return status
        if timeout_left > 0:
            time.sleep(poll_interval)
        timeout_left -= poll_interval

    return None


def activate_endpoint(api, endpoint_name):
    code, reason, result = api.endpoint_autoactivate(endpoint_name,
                                                     if_expires_in=600)
    if result["code"].startswith("AutoActivationFailed"):
        print "Auto activation failed, ls and transfers will likely fail!"
    # print "result: %s (%s)" % (result["code"], result["message"])


# This function currently transfers the CDE package and not the docker image

def globus_transfer(package_id, cfg,catalog_id, sciunit_id, datasetClient, db):
    #Obtain package from package directory 
    packages_directory = os.path.join(os.path.expanduser("~"), ".sciunit","packages")
    package_directory = os.path.join(packages_directory, package_id)

    if not os.path.isdir(package_directory):
        print "Cannot find image "+package_id+" for transfer; please run --package level collaboration"
        return

    # read Globus configuration from config.ini
    if not cfg.config['GLOBUS']['local-folder']:
        print "Cannot obtain from config file: local folder shared in GLOBUS "
        return
    if not cfg.config['GLOBUS']['local-endpoint']:
        print "Cannot obtain from config file: GLOBUS local endpoint"
        return
    if not cfg.config['GLOBUS']['remote-endpoint']:
        print "Cannot obtain from config file: GLOBUS remote endpoint"
        return
    if not cfg.config['GLOBUS']['globus-remote-folder']:
        print "Cannot obtain from config file: shared globus folder in remote GLOBUS endpoint"
        return
    if not cfg.config['GLOBUS']['globus-local-folder']:
        print "Cannot obtain from config file: shared globus folder in local GLOBUS endpoint"
        return

    print "Please wait. Saving image ..."

    # Zip the package
    short_package_name = package_id.strip()+".tgz"
    package_file = cfg.config['GLOBUS']['local-folder']+"/"+short_package_name
    command = "cd "+packages_directory+";tar czf "+package_file+" "+ package_id
    run_command(command)

    # Init Transfer
    source_endpoint  = cfg.config['GLOBUS']['local-endpoint'] # 'globuspublish#cirlab'
    source_folder= cfg.config['GLOBUS']['globus-local-folder'] # '/globuspublication_52/'
    remote_endpoint  = cfg.config['GLOBUS']['remote-endpoint'] # 'globuspublish#cirlab'
    remote_folder= cfg.config['GLOBUS']['globus-remote-folder'] # '/globuspublication_52/'

    # # command = "ssh cvlaescx@cli.globusonline.org scp cvlaescx#test_2:/docker_image/dockeraf8db02049ae25d3e64436769411206258b3b003.tar cvlaescx#test:/home/cristian/Installs/new3.tar"
    # source = "%s%s%s" %(source_endpoint,source_folder, short_image_name)
    # destination = "%s%s%s" %(remote_endpoint,remote_folder, short_image_name)
    # command = "ssh %s@cli.globusonline.org transfer -- %s %s" % (cfg.config['Default']['uname'], source, destination)
    # # print "will run: ",command
    # run_command(command)
    # #print "USAGE: transfer [destination_endpoint:destination_folder]"

    transfer_args = [cfg.config['Default']['uname'], '-g', cfg.config['Default']['goauth-token']]

    TransferAPIClient, _ = create_client_from_args(transfer_args)

    activate_endpoint(TransferAPIClient, source_endpoint)
    activate_endpoint(TransferAPIClient, remote_endpoint)

    # submit the transfer
    code, message, data = TransferAPIClient.transfer_submission_id()
    submission_id = data["value"]
    deadline = datetime.utcnow() + timedelta(minutes=10)
    t = Transfer(submission_id, source_endpoint, remote_endpoint, deadline)
    source_packagee_name = "%s%s"%(source_folder,short_package_name)
    destination_package_name = "%s%s"%(remote_folder,short_package_name)
    t.add_item(source_packagee_name, destination_package_name)

    code, reason, data = TransferAPIClient.transfer(t)
    task_id = data["task_id"]
    print "Source file:", source_package_name
    print "Server destination:", destination_package_name
    print "Submitted transfer id: ", task_id

    # see the new transfer show up
    print "=== After submit ==="
    display_task(TransferAPIClient, task_id, False); print

    # wait for the task to complete, and see the tasks and
    # endpoint ls change
    status = wait_for_task(TransferAPIClient,task_id)

    if status is None:
        # Task didn't complete before the timeout.
        # Since the example transfers a single small file and the
        # timeout is 2 mintues, this shouldn't happen unless one of the
        # endpoints is having problems or the user already has a bunch
        # of other active tasks (there is a limit to the
        # number of concurrent active tasks a user can have).
        print "WARNING: task did not complete before timeout!"
    else:
        print "Task %s complete with status %s" % (task_id, status)
        print "=== After completion ==="
        display_task(TransferAPIClient, task_id); print
	
	# Adding the transfered package to the geounit through add_member command	
    	add_file_raw_cmd = "--add_member " + source_package_name
    	add_file_cmd = SafeList(add_file_raw_cmd.split())
    	parse_cmd_add_member(add_file_cmd, catalog_id, geounit_id, datasetClient, db)

	# Setting transfer status = 1 in packages.json
	try:
          with open(packages_json_file) as data_file:
            packages_json = json.load(data_file)	
	except Exception as e:
	  print "Cannot open packages.json"
	  sys.stderr.write(str(e) + "\n")

	packages_json[package_id]['transfer'] = 1;
	with open(packages_json_file, 'w') as outfile:
            json.dump(packages_json, outfile, sort_keys = True, indent = 4)

    return "globus://%s%s%s"%(remote_endpoint,remote_folder,short_package_name)

def parse_cmd_transfer(cmd_splitted, package_hash=None, cfg=None):
  
    package_hash = cmd_splitted.get(1,UNDEFINED)
    globus_transfer(package_hash, cfg)
