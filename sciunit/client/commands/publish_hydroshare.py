import hs_restclient as hsrc
import getpass

def config_file_operations_hydroshare():
    """
    input: none
    output: none
    description: function edits the config.ini file in the .scids folder at user level. 
                it creates a 'HYDROSHARE' section in the config file
                and writes the Hydroshare token to it. 
		Once added, the same token will be used until it expires.
    """
    config_file_name = os.path.join(os.path.expanduser("~"),'.scids','config.ini')    
    config = configparser.ConfigParser()
    config.read(config_file_name)
    if 'HYDROSHARE' not in config.sections():
       hs_user_name, hs_auth = login()  
       config['HYDROSHARE'] = {'hs_user_name': hs_user_name,
                               'hs_auth': hs_auth}
       with open(config_file_name, 'w') as config_file:
            config.write(config_file)
    d = dict(config.items('HYDROSHARE'))
    return d['hs_auth'] 

def hs_credentials(hs_user_name,hs_passwd):
    """
    input:username, passwd
    output:authorization token
    """
    auth = hsrc.HydroShareAuthBasic(username=user_name, password=passwd) 
    return auth

def login():
    """
    input: none
    output: returns user name and password
    """
    user_name = raw_input('User name: ')
    passwd  = getpass.getpass()
    hs_auth = hs_credentials(hs_user_name,hs_passwd)
    return user_name, hs_auth

def hs_client(hs_auth):
    """
    input: hydroshare autherization token.
    output: hs-hydroshare rest_api module linked tio specific user. 
    """
    hs = hsrc.HydroShare(auth=auth)
    return hs

def create_new_resource(hs, title_as_str, path_to_zipped_geounit):
    """
    input: hs-hydroshare rest_api module, name of resource, path to the compressed geounit.
    output: creates a new resource on hydroshare. resource title is the
            name of the geounit on local client. option is given to make 
            this hydroshare resource private/public 
    """
    abstract = raw_input('enter abstract: ')
    title = raw_input('title: ')
    keywds = create_keywords()
	
    rtype = 'GenericResource' #see function resource_type()
    fpath = path_to_zipped_geounit
    resource_id = hs.createResource(rtype, title, resource_file=fpath)
    answ = raw_input('do you want to make this resource public (y/n): ')
    if answ == 'y':
       hs.setAccessRules(str(resource_id), public=True)
    else:
       hs.setAccessRules(str(resource_id), public=False)
    print "resources was created"
    os.system("rm -r "+fpath)

    #return the resource_id 
    return resource_id 

def package_to_tar_file_path(package_hash_id):
    """
    input: package hash-id as string
    output: path as string to the tar-file
    description: takes a package hash-id string, locates the package, 
    tar.gz this package and returns the path to the tar.gz file as a string.
    """
    packages_directory = os.path.join(os.path.expanduser("~"), ".scids","packages")
    package_directory = os.path.join(packages_directory, package_id)
    short_package_name = package_id.strip()+".tgz"
    package_file = packages_directory + "/" + short_package_name	
    command = "cd "+packages_directory+";tar -czf "+ package_file + " " + package_directory
    run_command(command)
	
    return package_file


def create_keywords():
    keywords = []
    n = raw_input('number of keywords to be added to the resource: ') 
    for i in range(1, int(n)+1):
        keywords.append(raw_input('keyword '+str(i)+":"))
    return keywords



def list_of_all_resources(hs, my_user_name):
    """
    input: hs-hydroshare rest_api module, user name 
    output: list if lists, where each sublist is [resource title, resource id]
            on hydroshare. resource title is equivalent to geounit name 
            and resource id is the hydroshare hash id, use to be determined later. 
    """
    L = []
    print ">>>>>>>", hs
    print ">>>>>>>", type(hs)
    G = hs.getResourceList(creator=my_user_name) #G is a Python generator
    for g in G:
        print g
        L.append([g[u'resource_title'], g[u'resource_id']])
    #return L
        

def resource_type():
    """
    input: none
    output: none
    description: this function gives a choice to the user to select appropriate resource ype during the resource creation process.
    """
    L = ['GenericResource','ModelInstanceResource','ModelProgramResource','NetcdfResource','RasterResource','RefTimeSeries','SWATModelInstanceResource','TimeSeriesResource','ToolResource']
    print ""
    print "the following resource types are available."
    for i in range(len(L)):
        print str(i+1)+") "+L[i]
    k = raw_input('select by choosing a numerical value: ')
    return L[int(k)-1]


