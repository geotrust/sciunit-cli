import os
import time
from setuptools import setup
import distutils.cmd


BASEPATH = os.path.dirname(os.path.abspath(__file__))
BINPATH = os.path.join(BASEPATH,'sciunit/client/commands/bin')

class GlobusCommand(distutils.cmd.Command):
      user_options = [ ('globusfile=', None, 'Globus Connect Personal install file name'), ('globusdirectory=',None,'Globus Connect Personal install directory'),
		
      ]

      def initialize_options(self):
        """Set default values for options."""
        # Each user option must be listed here with their default value.
        self.globusfile = '%s/globusconnectpersonal-latest.tgz'% BINPATH
        self.globusdirectory = '%s/globusconnectpersonal-latest*'%BINPATH

      def finalize_options(self):
        """Post-process options."""
	"""None for now"""

      def run(self):
	if (os.path.isfile(self.globusfile)) and (os.path.exists(self.globusdirectory)):
	   pass
	else:
	   cmd_to_run = 'wget https://s3.amazonaws.com/connect.globusonline.org/linux/stable/globusconnectpersonal-latest.tgz -O %s'% self.globusfile
	   os.system(cmd_to_run)
	   time.sleep(3)
	   cmd_to_run = 'tar xzf %s -C %s' % (self.globusfile,BINPATH)
	   os.system(cmd_to_run)
	   cmd_to_run = ' rm -rf %s' % (self.globusfile)
	   os.system(cmd_to_run)


#Does not work

def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
	    print "Printing paths " + paths
    return paths


globuspersonal = package_files('sciunit/client/commands/bin/globusconnectpersonal*')


setup(name="sciunit-client",
      version="1.0",
      description="Command-line client for creating sciunits",
      long_description=open("README.rst").read(),
      author="Cristian Vlaescu, Tanu Malik",
      author_email="scidataspace@gmail.com",
      url="https://bitbucket.org/geotrust/sciunit-cli",
      install_requires=['leveldb>=0.193','configparser>=3.3.0r2', 'docker-py>=1.2.2','requests>=2.13','globusonline-transfer-api-client>=0.10.11','hs_restclient>=1.2.6'],
      include_package_data=True,
      zip_safe=False,
      packages=["sciunit",
                "sciunit.client",
                "sciunit.client.globusonline",
                "sciunit.client.globusonline.catalog",
                "sciunit.client.globusonline.catalog.client",
                "sciunit.client.globusonline.catalog.client.ca",
                "sciunit.client.globusonline.catalog.client.examples",
		"sciunit.client.commands"],
      package_data={
      		"sciunit.client":[".sciunit/*"],
      		"sciunit.client.globusonline.catalog.client": ["ca/*.pem"],
		"sciunit.client.commands":["bin/ptu"],
		#'sciunit.client.commands':['bin/globus*']},
		#"sciunit.client.commands":["bin/globus*"]
		},
      scripts=["sciunit/client/sciunit-cli.py"], 
      keywords=["sciunit","globusonline"],
      classifiers=[
          "Development Status :: 3 - Alpha",
          "Intended Audience :: Users",
          "License :: OSI Approved :: Apache Software License",
          "Operating System :: MacOS :: MacOS X",
          "Operating System :: Microsoft :: Windows",
          "Operating System :: POSIX",
          "Programming Language :: Python",
          "Topic :: Communications :: File Sharing",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Software Development :: Libraries :: Python Modules",
          ],
      cmdclass={
          'globus': GlobusCommand,
        },
      )
